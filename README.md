# TSF-GRIPMAR21
This is a website for SAW NGO, which asks for donation for stray animal welfare.

Task 4 - Payment gateway integration

* This is the website I created as my task
* This is a simple website which allows user to make donations
* In this I learned to integrate a payment gateway and CSS animations

Tech Stack - HTML, CSS

This was created in March 2021

Demo video - https://www.youtube.com/watch?v=-rA_Ozh_lJQ&feature=youtu.be

Post - https://www.linkedin.com/posts/activity-6779102146551005184-c5-h
